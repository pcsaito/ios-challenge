//
//  User.swift
//  ios-challenge
//
//  Created by Paulo Saito on 13/05/17.
//  Copyright © 2017 pcsaito. All rights reserved.
//

import Foundation
import Mapper

extension GitHub {
    
    public struct UserProfile {
        let id: Int
        public let login: String
        public let name: String?
        public let avatarUrl: String
    }
}

extension GitHub.UserProfile: Mappable {
    
    public init(map: Mapper) throws {
        self.id = try map.from("id")
        self.login = try map.from("login")
        self.name = try? map.from("name") ?? ""
        self.avatarUrl = try map.from("avatar_url")
    }
}

extension GitHub.UserProfile: Equatable {
    
    public static func == (lhs: GitHub.UserProfile, rhs: GitHub.UserProfile) -> Bool {
        return lhs.id == rhs.id &&
            lhs.login == rhs.login &&
            lhs.name == rhs.name &&
            lhs.avatarUrl == rhs.avatarUrl
    }
}
