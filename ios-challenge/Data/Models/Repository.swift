//
//  Repository.swift
//  ios-challenge
//
//  Created by Paulo Saito on 13/05/17.
//  Copyright © 2017 pcsaito. All rights reserved.
//

import Foundation
import Mapper

extension GitHub {
    
    public struct Repository: hasAuthorType {
        let id: Int
        public let name: String
        public let fullName: String
        public let description: String
        public var author: UserProfile
        public let starCount: Int
        public let forksCount: Int
    }
    
    public struct Repositories {
        public let items: [Repository]
        public let repositoryCount: Int
    }

}

extension GitHub.Repository: Mappable {
    
    public init(map: Mapper) throws {
        self.id = try map.from("id")
        self.name = try map.from("name")
        self.fullName = try map.from("full_name")
        self.description = try map.from("description")
        self.author = try map.from("owner")
        self.starCount = try map.from("stargazers_count")
        self.forksCount = try map.from("forks_count")
    }
}

extension GitHub.Repositories: Mappable {
    
    public init(map: Mapper) throws {
        self.items = map.optionalFrom("items") ?? []
        self.repositoryCount = try map.from("total_count")
    }
}

extension GitHub.Repository: Equatable {
    
    public static func == (lhs: GitHub.Repository, rhs: GitHub.Repository) -> Bool {
        return lhs.id == rhs.id &&
            lhs.name == rhs.name &&
            lhs.fullName == rhs.fullName &&
            lhs.description == rhs.description &&
            lhs.author == rhs.author &&
            lhs.starCount == rhs.starCount &&
            lhs.forksCount == rhs.forksCount
    }
}
