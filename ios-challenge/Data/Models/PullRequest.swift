//
//  PullRequest.swift
//  ios-challenge
//
//  Created by Paulo Saito on 13/05/17.
//  Copyright © 2017 pcsaito. All rights reserved.
//

import Foundation
import Mapper


extension GitHub {
    
    public struct PullRequest: hasAuthorType {
        public enum PRState: String {
            case Open = "open"
            case Closed = "closed"
        }

        let id: Int
        public var author: UserProfile
        public let title: String
        public let createDate: Date
        public let updateDate: Date
        public let body: String
        public let state: PRState
        public let url: URL
    }
}

extension GitHub.PullRequest: Mappable {
    private static func formattedDate(dateString: String) throws -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        let formattedDate = dateFormatter.date(from: dateString)
        if let date = formattedDate {
            return date
        }
        
        throw MapperError.convertibleError(value: dateString, type: String.self)
    }

    public init(map: Mapper) throws {
        self.id = try map.from("id")
        self.author = try map.from("user")
        self.title = try map.from("title")
        
        let createdDateString: String = try map.from("created_at")
        self.createDate = try GitHub.PullRequest.formattedDate(dateString: createdDateString)

        let updateDateString: String = try map.from("updated_at")
        self.updateDate = try GitHub.PullRequest.formattedDate(dateString: updateDateString)

        self.body = try map.from("body")
        self.state = try map.from("state")
        self.url = try map.from("html_url")
    }
}

extension GitHub.PullRequest: Equatable {
    
    public static func == (lhs: GitHub.PullRequest, rhs: GitHub.PullRequest) -> Bool {
        return lhs.id == rhs.id &&
            lhs.author == rhs.author &&
            lhs.title == rhs.title &&
            lhs.createDate == rhs.createDate &&
            lhs.body == rhs.body &&
            lhs.state == rhs.state &&
            lhs.url == rhs.url
    }
}
