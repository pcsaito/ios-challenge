//
//  GitHub.swift
//  ios-challenge
//
//  Created by Paulo Saito on 13/05/17.
//  Copyright © 2017 pcsaito. All rights reserved.
//
import UIKit
import Mapper
import Moya

public struct GitHub {
    let provider: MoyaProvider<GitHubTarget>
    
    init() {
        #if DEBUG
            if let _ = NSClassFromString("XCTest") {
                self.provider = MoyaProvider<GitHubTarget>(stubClosure: MoyaProvider.immediatelyStub)
                return
            }
        #endif
        
        self.provider = MoyaProvider<GitHubTarget>()
    }
    
    func getRepositories(page: Int,
                         completion: @escaping ((_ repositories: GitHub.Repositories) -> Void),
                         onError: @escaping ((_ errorMessage: String) -> Void))
    {
        provider.request(.repositoriesList(page: page)) { result in
            switch result {
            case let .success(moyaResponse):
                let jsonDictionary = try! moyaResponse.mapJSON() as? NSDictionary
                if let json = jsonDictionary,
                    let repositories = Repositories.from(json) {
                    completion(repositories)
                }
                
            case let .failure(error):
                onError(error.localizedDescription)
            }
        }
    }
    
    func getPullRequests(repository: Repository,
                         completion: @escaping ((_ pullResquestList: [GitHub.PullRequest]) -> Void),
                         onError: @escaping ((_ errorMessage: String) -> Void))
    {
        provider.request(.pullRequestsList(repository: repository)) { result in
            switch result {
            case let .success(moyaResponse):
                if let jsonArray = try! moyaResponse.mapJSON() as? NSArray,
                    let pullRequestList = PullRequest.from(jsonArray) {
                    completion(pullRequestList)
                }
            case let .failure(error):
                onError(error.localizedDescription)
            }
        }
    }
    
    func getUserProfile(login: String,
                        completion: @escaping ((_ userProfile: GitHub.UserProfile) -> Void),
                        onError: @escaping ((_ errorMessage: String) -> Void))
    {
        provider.request(.userProfile(login: login)) { result in
            switch result {
            case let .success(moyaResponse):
                if let profileDictionary = try! moyaResponse.mapJSON() as? NSDictionary,
                    let profile = UserProfile.from(profileDictionary) {
                    completion(profile)
                }
            case let .failure(error):
                onError(error.localizedDescription)
            }
        }
    }
}
