//
//  GitHubTarget.swift
//  ios-challenge
//
//  Created by Paulo Saito on 13/05/17.
//  Copyright © 2017 pcsaito. All rights reserved.
//

import Foundation
import Moya

public enum GitHubTarget {
    case repositoriesList(page: Int)
    case pullRequestsList(repository: GitHub.Repository)
    case userProfile(login: String)
}

extension GitHubTarget: TargetType {
    
    public var baseURL: URL {
        return URL(string: "https://api.github.com/")!
    }
    
    public var path: String {
        switch self {
            
        case .repositoriesList:
            return "search/repositories"
            
        case .pullRequestsList(let repository):
            return "repos/\(repository.fullName)/pulls"
            
        case .userProfile(let login):
            return "users/\(login)"
        }
    }
    
    public var method: Moya.Method {
        switch self {
            
        case .repositoriesList:
            return .get
            
        case .pullRequestsList:
            return .get
            
        case .userProfile:
            return .get
        }
    }
    
    public var parameters: [String: Any]? {
        switch self {
            
        case .repositoriesList(let page):
            return ["q":"language:Swift" , "sort":"stars" , "page":page]
            
        case .pullRequestsList:
            return nil
            
        case .userProfile:
            return nil
        }
    }
    
    public var parameterEncoding: ParameterEncoding {
        switch self {
        case .repositoriesList:
            return URLEncoding.queryString
            
        default:
            return URLEncoding.default
        }
    }
    
    public var task: Task {
        switch self {
            
        default:
            return .request
        }
    }
    
    public var sampleData: Data {
        switch self {
            
        case .repositoriesList:
            return stubbedResponse(filename: "repositoriesListStub")
            
        case .pullRequestsList:
            return stubbedResponse(filename: "pullRequestsListStub")
            
        case .userProfile:
            return stubbedResponse(filename: "userStub")
        }
    }
}

extension GitHubTarget {
    
    func stubbedResponse(filename: String) -> Data {
        guard let path = Bundle.main.path(forResource: filename, ofType: "json") else {
            fatalError("Could not find a stubbed archive with filename: \(filename)")
        }
        do {
            return try NSData(contentsOf: URL(fileURLWithPath: path)) as Data
        } catch {
            fatalError("Could not load a stubbed response with filename: \(filename)\n" +
                "At path: \(path)\nError: \(error.localizedDescription)")
        }
    }
}
