//
//  PullRequestsViewModel.swift
//  ios-challenge
//
//  Created by Paulo Saito on 14/05/17.
//  Copyright © 2017 pcsaito. All rights reserved.
//

import Foundation

class PullRequestsViewModel {
    let gitHubAPI = GitHub()
    let repository: GitHub.Repository
    var pullRequestCellModels: [PullRequestCellModel] = []
    
    private var page: Int = 1
    private var pullRequestList: [GitHub.PullRequest] = [] {
        didSet {
            self.pullRequestCellModels = pullRequestList.map({ value in
                return PullRequestCellModel(model: value)
            })
        }
    }
    
    init(repository: GitHub.Repository) {
        self.repository = repository
    }
    
    
    var title: String {
        return repository.name
    }
    
    var closedPR: String {
        let closedPR = pullRequestList.filter({(pullRequest) in
            return Bool(pullRequest.state == GitHub.PullRequest.PRState.Closed)
        })
        
        return "\(closedPR.count) closed"
    }
    
    var openPR: String {
        let openPR = pullRequestList.filter({(pullRequest) in
            return Bool(pullRequest.state == GitHub.PullRequest.PRState.Open)
        })
        
        return "\(openPR.count) open"
    }
    
    
    func pullRequestURL(index: Int) -> URL {
        return self.pullRequestList[index].url
    }
    
    
    func loadMore(completion: @escaping () -> Void,
                  onError: @escaping ((_ errorMessage: String) -> Void))
    {
        gitHubAPI.getPullRequests(repository: self.repository, completion:{ [unowned self] (pullRequests: [GitHub.PullRequest]) in
            self.pullRequestList = pullRequests

            completion()
        },  onError: { (errorMessage) in
                onError(errorMessage)
        })
    }    
}
