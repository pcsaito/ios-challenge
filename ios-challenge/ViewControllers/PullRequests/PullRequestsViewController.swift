//
//  PullRequestsViewController.swift
//  ios-challenge
//
//  Created by Paulo Saito on 14/05/17.
//  Copyright © 2017 pcsaito. All rights reserved.
//

import UIKit
import SafariServices

class PullRequestsViewController: UITableViewController, ErrorDisplayable {
    @IBOutlet weak var openPRLabel: UILabel!
    @IBOutlet weak var closedPRLabel: UILabel!

    var viewModel: PullRequestsViewModel?
    fileprivate var cellModels: [PullRequestCellModel]? {
        return viewModel?.pullRequestCellModels
    }
    
    
    static func instantiateViewController(with viewModel: PullRequestsViewModel) -> PullRequestsViewController {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "PullRequestsViewController") as! PullRequestsViewController
        
        viewController.viewModel = viewModel
        viewController.title = viewModel.title
        
        return viewController
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureTableView()
        
        self.viewModel?.loadMore(completion: {
            self.reloadUI()
        }, onError: { (errorMessage) in
            self.onError(errorMessage: errorMessage)
        })
    }
    
    func configureTableView() {
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        tableView.backgroundView = activityIndicator
        activityIndicator.startAnimating()
    }
    
    func reloadUI() {
        self.tableView.reloadData()

        openPRLabel.text = viewModel?.openPR ?? ""
        closedPRLabel.text = viewModel?.closedPR ?? ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension PullRequestsViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (cellModels?.count) ?? 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 128
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"PullRequestCell", for: indexPath)
        if let repositoryCell = cell as? PullRequestCell {
            repositoryCell.model = self.cellModels?[indexPath.row]
        }
        
        return cell
    }
}

extension PullRequestsViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let pullRequestURL = self.viewModel?.pullRequestURL(index: indexPath.row) {
            let safariVC = SFSafariViewController(url: pullRequestURL)
            present(safariVC, animated: true, completion: nil)
        }
    }
}
