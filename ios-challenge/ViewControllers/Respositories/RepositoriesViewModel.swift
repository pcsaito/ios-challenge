//
//  RepositoriesViewModel.swift
//  ios-challenge
//
//  Created by Paulo Saito on 13/05/17.
//  Copyright © 2017 pcsaito. All rights reserved.
//

import Foundation

class RepositoriesViewModel {
    let gitHubAPI: GitHub
    var repositoryCellModels: [RepositoryCellModel] = []

    private var page: Int = 1
    private var repositoryList: [GitHub.Repository] = []
    private var repositories: GitHub.Repositories? {
        didSet {
            self.repositoryCellModels = repositories?.items.map({ value in
                return RepositoryCellModel(model: value)
            }) ?? []
        }
    }
    
    init(api: GitHub) {
        gitHubAPI = api
    }

    var repositoryCount: Int? {
        return self.repositories?.repositoryCount
    }
    
    
    func loadMore(completion: @escaping () -> Void,
        onError: @escaping ((_ errorMessage: String) -> Void))
    {
    
        gitHubAPI.getRepositories(page:page, completion: { [unowned self] (repositories: GitHub.Repositories) in
            self.page += 1
            self.repositories = repositories
            let allRepositories = self.repositoryList + repositories.items
            self.repositoryList = allRepositories.reduce([]) { $0.contains($1) ? $0 : $0 + [$1] }
            
            completion()
        }, onError: { (errorMessage) in
            onError(errorMessage)
        })
    }
    
    func pullRequestViewModelForRow(index: Int) -> PullRequestsViewModel {
        let repository = self.repositoryList[index]
        return PullRequestsViewModel.init(repository: repository)
    }
}
