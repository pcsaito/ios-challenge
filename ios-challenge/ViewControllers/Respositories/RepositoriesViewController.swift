//
//  RespositoriesViewController.swift
//  ios-challenge
//
//  Created by Paulo Cesar Saito on 5/12/17.
//  Copyright © 2017 pcsaito. All rights reserved.
//

import UIKit

class RepositoriesViewController: UITableViewController, ErrorDisplayable {
    fileprivate var viewModel: RepositoriesViewModel
    fileprivate var cellModels: [RepositoryCellModel] {
        return viewModel.repositoryCellModels
    }
    
    required init?(coder aDecoder: NSCoder) {
        viewModel = RepositoriesViewModel(api: GitHub())
        
        super.init(coder: aDecoder)
    }
    
	override func viewDidLoad() {
		super.viewDidLoad()
        
        self.viewModel.loadMore(completion: {
            self.tableView.reloadData()
        }, onError: { (errorMessage) in
            self.onError(errorMessage: errorMessage)
        })
    }

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
    }
}

extension RepositoriesViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellModels.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 128
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"RepositoryCell", for: indexPath)
        if let repositoryCell = cell as? RepositoryCell {
            let cellModel = self.cellModels[indexPath.row]
            repositoryCell.model = cellModel
        }
        
        if let repositoryCount = self.viewModel.repositoryCount {
            if (indexPath.row > cellModels.count-10 && indexPath.row < repositoryCount-1) {
                self.viewModel.loadMore(completion: {
                    self.tableView.reloadData()
                }, onError: { (errorMessage) in
                    self.onError(errorMessage: errorMessage)
                })
            }
        }
        
        return cell
    }
}

extension RepositoriesViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let prViewModel = self.viewModel.pullRequestViewModelForRow(index: indexPath.row)
        let prViewController = PullRequestsViewController.instantiateViewController(with: prViewModel)
        self.navigationController?.pushViewController(prViewController, animated: true)
    }
}
