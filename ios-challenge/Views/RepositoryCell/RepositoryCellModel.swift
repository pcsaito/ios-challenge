//
//  RepositoryViewModel.swift
//  ios-challenge
//
//  Created by Paulo Saito on 14/05/17.
//  Copyright © 2017 pcsaito. All rights reserved.
//

import Foundation

struct RepositoryCellModel: UserProfileLoadable {
    var model: hasAuthorType

    let name: String
    let description: String
    let forkCount: String
    let starCount: String
    let authorLogin: String
    let authorName: String
    let authorAvatarUrl: URL?
    
    init(model: GitHub.Repository) {
        self.model = model
        
        self.name = model.name
        self.description = model.description
        self.forkCount = String(model.forksCount)
        self.starCount = String(model.starCount)
        
        self.authorLogin = model.author.login
        self.authorName = model.author.name ?? ""
        self.authorAvatarUrl = URL(string: model.author.avatarUrl)
    }
}
