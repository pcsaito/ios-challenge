//
//  RepositoryCell.swift
//  ios-challenge
//
//  Created by Paulo Saito on 13/05/17.
//  Copyright © 2017 pcsaito. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

public class RepositoryCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var forkCountLabel: UILabel!
    @IBOutlet weak var starCountLabel: UILabel!
    @IBOutlet weak var authorLoginLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var authorAvatarImage: UIImageView!
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        self.configureView()
    }
    
    func configureView() {
        self.authorAvatarImage.kf.indicatorType = .activity
        self.authorAvatarImage.layer.cornerRadius = self.authorAvatarImage.frame.size.width / 2
        self.authorAvatarImage.clipsToBounds = true

    }

    var model: RepositoryCellModel? {
        didSet {
            if let cellModel = model {
                self.nameLabel.text = cellModel.name
                self.descriptionLabel.text = cellModel.description
                self.forkCountLabel.text = cellModel.forkCount
                self.starCountLabel.text = cellModel.starCount
                
                self.authorLoginLabel.text = cellModel.authorLogin
                self.authorNameLabel.text = cellModel.authorName
                
                if let url = cellModel.authorAvatarUrl {
                    let placeholderImage = UIImage(named: "avatarPlaceholder")
                    self.authorAvatarImage.kf.setImage(with: url, placeholder: placeholderImage)
                }
                
                cellModel.loadUserProfile(login: cellModel.authorLogin, completion: { [unowned self] (userProfile: GitHub.UserProfile) in
                    self.authorNameLabel.text = userProfile.name ?? ""
                })
            }
        }
    }
}
