//
//  PullRequestCell.swift
//  ios-challenge
//
//  Created by Paulo Saito on 14/05/17.
//  Copyright © 2017 pcsaito. All rights reserved.
//

import UIKit

public class PullRequestCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var authorLoginLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var authorAvatarImage: UIImageView!
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        self.configureView()
    }
    
    func configureView() {
        self.authorAvatarImage.kf.indicatorType = .activity
        self.authorAvatarImage.layer.cornerRadius = self.authorAvatarImage.frame.size.width / 2
        self.authorAvatarImage.clipsToBounds = true
    }
    
    var model: PullRequestCellModel? {
        didSet {
            if let cellModel = model {
                self.titleLabel.text = cellModel.title
                self.bodyLabel.text = cellModel.body
                
                self.authorLoginLabel.text = cellModel.authorLogin
                self.authorNameLabel.text = cellModel.authorName
                if let url = cellModel.authorAvatarUrl {
                    let placeholderImage = UIImage(named: "avatarPlaceholder")
                    self.authorAvatarImage.kf.setImage(with: url, placeholder: placeholderImage)
                }
                
                cellModel.loadUserProfile(login: cellModel.authorLogin, completion: { [unowned self] (userProfile: GitHub.UserProfile) in
                    self.authorNameLabel.text = userProfile.name ?? ""
                })
            }
        }
    }

}
