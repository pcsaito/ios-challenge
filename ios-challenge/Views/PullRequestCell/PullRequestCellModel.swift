//
//  PullRequestCellModel.swift
//  ios-challenge
//
//  Created by Paulo Saito on 14/05/17.
//  Copyright © 2017 pcsaito. All rights reserved.
//

import Foundation

struct PullRequestCellModel: UserProfileLoadable {
    var model: hasAuthorType

    let title: String
    let body: String
    let authorLogin: String
    var authorName: String
    let authorAvatarUrl: URL?
    
    init(model: GitHub.PullRequest) {
        self.model = model
        
        self.title = model.title
        self.body = model.body
        
        self.authorLogin = model.author.login
        self.authorName = model.author.name ?? ""
        self.authorAvatarUrl = URL(string: model.author.avatarUrl)
    }
}
