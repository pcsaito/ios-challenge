//
//  UserProfileLoadable.swift
//  ios-challenge
//
//  Created by Paulo Saito on 16/05/17.
//  Copyright © 2017 pcsaito. All rights reserved.
//

import Foundation

public protocol hasAuthorType {
    var author: GitHub.UserProfile { get set }
}

protocol UserProfileLoadable {
    var model: hasAuthorType { get }
    
    func loadUserProfile(login: String,
                         completion: @escaping (_ userProfile: GitHub.UserProfile) -> Void)
}

extension UserProfileLoadable {
    func loadUserProfile(login: String,
                         completion: @escaping (_ userProfile: GitHub.UserProfile) -> Void)
    {
        let gitHubAPI = GitHub()
        gitHubAPI.getUserProfile(login: model.author.login, completion: { (userProfile: GitHub.UserProfile) in
            completion(userProfile)
        }, onError: {_ in })
    }
}
