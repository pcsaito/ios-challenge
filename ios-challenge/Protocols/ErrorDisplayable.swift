//
//  ErrorDisplayable.swift
//  ios-challenge
//
//  Created by Paulo Saito on 16/05/17.
//  Copyright © 2017 pcsaito. All rights reserved.
//

import UIKit

protocol ErrorDisplayable {
    func onError(errorMessage: String)
}

extension ErrorDisplayable where Self: UIViewController {
    func onError(errorMessage: String) {
        let alertController = UIAlertController(title: "Error",
                                                message: errorMessage,
                                                preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Ok", style: .default)
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
}
