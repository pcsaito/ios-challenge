//
//  PullRequestsViewModelTests.swift
//  ios-challenge
//
//  Created by Paulo Saito on 16/05/17.
//  Copyright © 2017 pcsaito. All rights reserved.
//

import UIKit
import Alamofire
import Moya
import Quick
import Nimble

@testable import ios_challenge

class PullRequestsViewModelTests: QuickSpec {
    func stubbedResponse(filename: String) -> Data {
        guard let path = Bundle.main.path(forResource: filename, ofType: "json") else {
            fatalError("Could not find a stubbed archive with filename: \(filename)")
        }
        do {
            return try NSData(contentsOf: URL(fileURLWithPath: path)) as Data
        } catch {
            fatalError("Could not load a stubbed response with filename: \(filename)\n" +
                "At path: \(path)\nError: \(error.localizedDescription)")
        }
    }
    
    override func spec() {
        var pullRequestsViewModel: PullRequestsViewModel?
        
        describe("when mapping response") {
            beforeEach {
   
                let repositoryData = self.stubbedResponse(filename: "repositoryStub")
                let repositoryJson = try! JSONSerialization.jsonObject(with: repositoryData, options: []) as? NSDictionary
                
                let repository = GitHub.Repository.from(repositoryJson!)
                pullRequestsViewModel = PullRequestsViewModel(repository: repository!)
                pullRequestsViewModel?.loadMore(completion: { }, onError: { (errorMessage) in })

            }
            it("should format raw values correctly") {
                expect(pullRequestsViewModel!.title) == "Alamofire"
                expect(pullRequestsViewModel!.openPR) == "10 open"
                expect(pullRequestsViewModel!.closedPR) == "0 closed"
            }
            it("should have a valid cell model") {
                let cellModels = pullRequestsViewModel!.pullRequestCellModels
                let firstCellModel = cellModels[0]
                expect(firstCellModel.title) == "optional number getter fix"
                expect(firstCellModel.body) == "`case .string` is missed for optional `number` getter. I just put the same code from non-optional `number` getter, except that if NSDecimalNumber fails to parse string - it returns nil"
                expect(firstCellModel.authorLogin) == "pkcs12"
                expect(pullRequestsViewModel!.pullRequestURL(index: 0)) == URL(string: "https://github.com/SwiftyJSON/SwiftyJSON/pull/834")!
            }
        }
    }
}
