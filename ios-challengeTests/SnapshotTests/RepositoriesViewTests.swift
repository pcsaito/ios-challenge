//
//  RepositoriesViewTests.swift
//  ios-challenge
//
//  Created by Paulo Saito on 16/05/17.
//  Copyright © 2017 pcsaito. All rights reserved.
//

import Quick
import Nimble
import Nimble_Snapshots

@testable import ios_challenge

class RepositoriesViewTests: QuickSpec {
    
    override func spec() {
        
        describe("when rendering repositories view") {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let repositoriesViewController = storyBoard.instantiateViewController(withIdentifier: "RepositoriesViewController")
            
            it("should show correct cells and sections") {
                //expect(repositoriesViewController.view).to(recordSnapshot())
                expect(repositoriesViewController.view).to(haveValidSnapshot())
            }
        }
    }
}
