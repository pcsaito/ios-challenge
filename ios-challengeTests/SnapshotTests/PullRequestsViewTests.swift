//
//  PullRequestsViewTests.swift
//  ios-challenge
//
//  Created by Paulo Saito on 16/05/17.
//  Copyright © 2017 pcsaito. All rights reserved.
//

import Quick
import Nimble
import Nimble_Snapshots

@testable import ios_challenge

class PullRequestsViewTests: QuickSpec {
    func stubbedResponse(filename: String) -> Data {
        guard let path = Bundle.main.path(forResource: filename, ofType: "json") else {
            fatalError("Could not find a stubbed archive with filename: \(filename)")
        }
        do {
            return try NSData(contentsOf: URL(fileURLWithPath: path)) as Data
        } catch {
            fatalError("Could not load a stubbed response with filename: \(filename)\n" +
                "At path: \(path)\nError: \(error.localizedDescription)")
        }
    }
    
    override func spec() {
        
        describe("when rendering repositories view") {
            let repositoryData = self.stubbedResponse(filename: "repositoryStub")
            let repositoryJson = try! JSONSerialization.jsonObject(with: repositoryData, options: []) as? NSDictionary
            
            let repository = ios_challenge.GitHub.Repository.from(repositoryJson!)
            let viewModel = ios_challenge.PullRequestsViewModel(repository: repository!)
            viewModel.loadMore(completion: { }, onError: { (errorMessage) in })
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: "PullRequestsViewController") as! ios_challenge.PullRequestsViewController
            
            viewController.viewModel = viewModel
            viewController.title = viewModel.title

            it("should show correct cells and sections") {
                //expect(viewController.view).to(recordSnapshot())
                expect(viewController.view).to(haveValidSnapshot())
            }
        }
    }
}
