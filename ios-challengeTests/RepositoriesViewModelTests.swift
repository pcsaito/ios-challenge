//
//  RepositoriesViewModelTests.swift
//  ios-challenge
//
//  Created by Paulo Saito on 15/05/17.
//  Copyright © 2017 pcsaito. All rights reserved.
//

import UIKit
import Alamofire
import Moya
import Quick
import Nimble

@testable import ios_challenge

class RepositoriesViewModelTests: QuickSpec {
    
    override func spec() {
        var repositoriesViewModel: RepositoriesViewModel?
        let repositoryCount = 30
        let repositoryTotalCount = 261235

        describe("when mapping response") {
            beforeEach {
                repositoriesViewModel = RepositoriesViewModel(api: GitHub())
                repositoriesViewModel?.loadMore(completion: { }, onError: { (errorMessage) in })
            }
            it("should format raw values correctly") {
                expect(repositoriesViewModel!.repositoryCount).toEventually(equal(repositoryTotalCount), timeout:5)
                expect(repositoriesViewModel!.repositoryCellModels.count).toEventually(equal(repositoryCount), timeout:5)
            }
            it("should have a valid cell model") {
                let cellModels = repositoriesViewModel!.repositoryCellModels
                let firstCellModel = cellModels[0]
                expect(firstCellModel.name) == "Alamofire"
                expect(firstCellModel.description) == "Elegant HTTP Networking in Swift"
                expect(firstCellModel.authorLogin) == "Alamofire"
                expect(firstCellModel.forkCount) == "4052"
                expect(firstCellModel.starCount) == "23454"
                expect(firstCellModel.authorAvatarUrl!.absoluteString).to(equal("https://avatars0.githubusercontent.com/u/7774181?v=3"))
            }
        }
    }
}
